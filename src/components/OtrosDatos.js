import React from "react";
import { useForm } from "react-hook-form";

function OtrosDatos(props) {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = data => {
    props.setManejo({
        post_suspension: data.suspension, 
        re_administracion: data.readmin
    });
    props.setResultado({
        secuela: data.secuela,
        muerte: data.muerte
    });
    props.setGravedad({
        grav_hospitalizacion: data.grav_hospitalizacion,
        grav_riesgo: data.grav_riesgo,
        grav_incapacidad: data.grav_incapacidad,
        grav_muerte: data.grav_muerte,
        grav_anomalia: data.grav_anomalia,
        grav_otros: data.grav_otros
    });
    props.setAntecedentes(data.antecedentes);
    props.setPaso(6);
  };
  
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <br/>
      <div className="card">
        <h5 className="card-header">Manejo del evento adverso</h5>
        <div className="card-body">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label>La reacción desapareció luego de la suspensión del medicamento</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="suspension" 
                    id="suspension_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Manejo.post_suspension === 'Si'} 
                />
                    <label className="form-check-label" htmlFor="suspension_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="suspension" 
                        id="suspension_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Manejo.post_suspension === 'No'} 
                    />
                    <label className="form-check-label" htmlFor="suspension_no">
                        No
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="suspension" 
                        id="suspension_na" 
                        value="n/a" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Manejo.post_suspension === 'n/a'} 
                    />    
                    <label className="form-check-label" htmlFor="suspension_na">
                        N/A
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.suspension?.message}
                </span>
            </div>
            <div className="form-group col-md-6">
              <label>La reacción apareció luego de la re-administración del medicamento</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="readmin" 
                    id="readmin_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Manejo.re_administracion === 'Si'} 
                />
                    <label className="form-check-label" htmlFor="readmin_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="readmin" 
                        id="readmin_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Manejo.re_administracion === 'No'} 
                    />
                    <label className="form-check-label" htmlFor="readmin_no">
                        No
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="readmin" 
                        id="readmin_na" 
                        value="n/a" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Manejo.re_administracion === 'n/a'} 
                    />
                    <label className="form-check-label" htmlFor="readmin_na">
                        N/A
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.readmin?.message}
                </span>
            </div>
        </div>
    </div>
    </div>

    <br/>
      <div className="card">
        <h5 className="card-header">Resultado de la reacción adversa</h5>
        <div className="card-body">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label>Descripción de la recuperación del paciente</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="secuela" 
                    id="secuela_si" 
                    value="Recuperado con secuela" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Resultado.secuela === 'Recuperado con secuela'}
                />
                    <label className="form-check-label" htmlFor="secuela_si">
                        Recuperado con secuela
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="secuela" 
                        id="secuela_no" 
                        value="Recuperado sin secuela" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Resultado.secuela === 'Recuperado sin secuela'}
                    />
                    <label className="form-check-label" htmlFor="secuela_no">
                        Recuperado sin secuela
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="secuela" 
                        id="secuela_na" 
                        value="No recuperado" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Resultado.secuela === 'No recuperado'}
                    />
                    <label className="form-check-label" htmlFor="secuela_na">
                        No recuperado
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.secuela?.message}
                </span>
            </div>
            <div className="form-group col-md-6">
              <label>Descripción de la muerte del paciente</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="muerte" 
                    id="muerte_si" 
                    value="Muerte asociada a la RAM" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Resultado.muerte === 'Muerte asociada a la RAM'}
                />
                    <label className="form-check-label" htmlFor="muerte_si">
                        Muerte asociada a la RAM
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="muerte" 
                        id="muerte_no" 
                        value="Muerte no asociada a la RAM" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Resultado.muerte === 'Muerte no asociada a la RAM'}
                    />
                    <label className="form-check-label" htmlFor="muerte_no">
                        Muerte no asociada a la RAM
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="muerte" 
                        id="muerte_na" 
                        value="No sabe" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Resultado.muerte === 'No sabe'}
                    />
                    <label className="form-check-label" htmlFor="muerte_na">
                        No sabe
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.muerte?.message}
                </span>
            </div>
        </div>
    </div>
    </div>

    <br/>
      <div className="card">
        <h5 className="card-header">Gravedad de la RAM</h5>
        <div className="card-body">
          <div className="form-row">
            <div className="form-group col-md-4">
              <label>Produjo o prolongo la hospitalización del paciente</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="grav_hospitalizacion" 
                    id="grav_hospitalizacion_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Gravedad.grav_hospitalizacion === 'Si'}
                />
                    <label className="form-check-label" htmlFor="grav_hospitalizacion_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="grav_hospitalizacion" 
                        id="grav_hospitalizacion_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Gravedad.grav_hospitalizacion === 'No'}
                    />
                    <label className="form-check-label" htmlFor="grav_hospitalizacion_no">
                        No
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.grav_hospitalizacion?.message}
                </span>
            </div>

            <div className="form-group col-md-4">
              <label>Pone en riesgo la vida del paciente</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="grav_riesgo" 
                    id="grav_riesgo_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Gravedad.grav_riesgo === 'Si'}
                />
                    <label className="form-check-label" htmlFor="grav_riesgo_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="grav_riesgo" 
                        id="grav_riesgo_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Gravedad.grav_riesgo === 'No'}
                    />
                    <label className="form-check-label" htmlFor="grav_riesgo_no">
                        No
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.grav_riesgo?.message}
                </span>
            </div>

            <div className="form-group col-md-4">
              <label>Incapacidad persistente o grave</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="grav_incapacidad" 
                    id="grav_incapacidad_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Gravedad.grav_incapacidad === 'Si'}
                />
                    <label className="form-check-label" htmlFor="grav_incapacidad_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="grav_incapacidad" 
                        id="grav_incapacidad_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Gravedad.grav_incapacidad === 'No'}
                    />
                    <label className="form-check-label" htmlFor="grav_incapacidad_no">
                        No
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.grav_incapacidad?.message}
                </span>
            </div>
        </div>

        <div className="form-row">
            <div className="form-group col-md-4">
              <label>Muerte del paciente</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="grav_muerte" 
                    id="grav_muerte_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Gravedad.grav_muerte === 'Si'}
                />
                    <label className="form-check-label" htmlFor="grav_muerte_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="grav_muerte" 
                        id="grav_muerte_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Gravedad.grav_muerte === 'No'}
                    />
                    <label className="form-check-label" htmlFor="grav_muerte_no">
                        No
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.grav_muerte?.message}
                </span>
            </div>
            
            <div className="form-group col-md-4">
              <label>Anomalía congénita</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="grav_anomalia" 
                    id="grav_anomalia_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Gravedad.grav_anomalia === 'Si'}
                />
                    <label className="form-check-label" htmlFor="grav_anomalia_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="grav_anomalia" 
                        id="grav_anomalia_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Gravedad.grav_anomalia === 'No'}
                    />
                    <label className="form-check-label" htmlFor="grav_anomalia_no">
                        No
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.grav_anomalia?.message}
                </span>
            </div>

            <div className="form-group col-md-4">
              <label>Otras</label>
              <div className="form-check">
                <input className="form-check-input" 
                    type="radio" 
                    name="grav_otros" 
                    id="grav_otros_si" 
                    value="Si" 
                    ref={register({
                    required: {
                        value: true, 
                        message: 'Campo es requerido'
                        }
                    })}
                    defaultChecked={props.Gravedad.grav_otros === 'Si'}
                />
                    <label className="form-check-label" htmlFor="grav_otros_si">
                        Si
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" 
                        type="radio" 
                        name="grav_otros" 
                        id="grav_otros_no" 
                        value="No" 
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Campo es requerido'
                            }
                        })}
                        defaultChecked={props.Gravedad.grav_otros === 'No'}
                    />
                    <label className="form-check-label" htmlFor="grav_otros_no">
                        No
                    </label>
                </div>
                <span className="text-danger">
                    {errors?.grav_otros?.message}
                </span>
            </div>
        </div>

    </div>
    </div>


        <br/>
      <div className="card">
        <h5 className="card-header">Antecedentes relevantes</h5>
        <div className="card-body">
        <div className="form-row">
            <div className="form-group col-md-12">
                <label htmlFor="antecedentes">Antecedentes</label>
                <textarea 
                className="form-control" 
                id="antecedentes" 
                name="antecedentes" 
                rows="4"
                ref={register({
                required: {
                    value: true, 
                    message: 'Campo es requerido'
                    }
                })}
                defaultValue={props.Antecedentes}
                >
                </textarea>
                <span className="text-danger">
                    {errors?.antecedentes?.message}
                </span>
            </div>
            </div>
        </div>
    </div>
  
    <br/>
    <div className="form-row">
        <div className="form-gruop">
            <div className="col-md-12">
                <button type="button" onClick={()=>props.setPaso(4)} className="btn btn-success">
                    Anterior
                </button>
            </div>
        </div>
        <div className="form-gruop">
            <div className="col-md-12">
                <button type="submit" className="btn btn-success">
                    Continuar
                </button>
            </div>
        </div>
    </div>

    <br/>



</form>
  );
}

export default OtrosDatos;