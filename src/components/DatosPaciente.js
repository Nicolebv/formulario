import React from "react";
import { useForm } from "react-hook-form";

function DatosPaciente(props) {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = data => {
    props.setPaciente(data);
    props.setPaso(2);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <br/>
      <div className="card">
        <h5 className="card-header">Datos del paciente</h5>
        <div className="card-body">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="paciente_nombre">Nombres</label>
              <input type="text" 
                    className="form-control" 
                    id="paciente_nombre" 
                    name="paciente_nombre" 
                    placeholder="Nombres"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Nombre es requerido'
                          }
                      })}
                    defaultValue={props.Paciente.paciente_nombre}
              />
              <span className="text-danger">
                  {errors?.paciente_nombre?.message}
              </span>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="apelpaciente_apellidolido">Apellidos</label>
              <input type="text" 
                    className="form-control" 
                    id="paciente_apellido" 
                    name="paciente_apellido" 
                    placeholder="Apellidos"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Apellido es requerido'
                          }
                      })}
                    defaultValue={props.Paciente.paciente_apellido}
              />
              <span className="text-danger">
                  {errors?.paciente_apellido?.message}
              </span>
            </div>
          </div>
          <div className="form-row">
            <fieldset className="form-group col-md-2">
              <div className="row">
                <legend className="col-form-label col-md-5">Sexo</legend>
                <div className="col-sm-10">
                  <div className="form-check">
                    <input className="form-check-input" 
                          type="radio" 
                          name="sexo" 
                          id="femenino" 
                          value="femenino"
                          ref={register({
                            required: {
                                value: true, 
                                message: 'Sexo es requerido'
                                }
                            })}
                          defaultChecked={props.Paciente.sexo === 'femenino'} 
                    />
                    <label className="form-check-label" htmlFor="femenino">
                      Femenino
                    </label>
                  </div>
                  <div className="form-check">
                    <input className="form-check-input" 
                          type="radio" 
                          name="sexo" 
                          id="masculino" 
                          value="masculino"
                          ref={register({
                            required: {
                                value: true, 
                                message: 'Sexo es requerido'
                                }
                            })}
                          defaultChecked={props.Paciente.sexo === 'masculino'} 
                    />
                    <label className="form-check-label" htmlFor="masculino">
                      Masculino
                    </label>
                  </div>
                  <span className="text-danger">
                      {errors?.sexo?.message}
                  </span>
                </div>
               
              </div>
            </fieldset>
            <div className="form-group col-md-2">
              <label htmlFor="nombre">Edad</label>
              <input type="text" 
                    className="form-control" 
                    id="edad" 
                    name="edad" 
                    placeholder="Edad"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Edad es requerida'
                          }
                      })}
                    defaultValue={props.Paciente.edad}
              />
              <span className="text-danger">
                  {errors?.edad?.message}
              </span>
            </div>
            <div className="form-group col-md-2">
              <label htmlFor="nombre">Peso</label>
              <input type="text" 
                      className="form-control" 
                      id="peso" 
                      name="peso" 
                      placeholder="Peso"
                      ref={register({
                        required: {
                            value: true, 
                            message: 'Peso es requerido'
                            }
                        })}
                      defaultValue={props.Paciente.peso}
              />
              <span className="text-danger">
                  {errors?.peso?.message}
              </span>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="historiaClinica">Historia clínica</label>
              <textarea
                className="form-control" 
                id="historiaClinica" 
                name="historiaClinica"
                rows="2"
                ref={register({
                  required: {
                      value: true, 
                      message: 'Historia clinica es requerida'
                      }
                  })}
                defaultValue={props.Paciente.historiaClinica}>
              </textarea>
              <span className="text-danger">
                  {errors?.historiaClinica?.message}
              </span>
            </div>
          </div>
          <button type="submit" className="btn btn-success">
            Continuar
          </button>
        </div>
      </div>
    </form>
  );
}

export default DatosPaciente;