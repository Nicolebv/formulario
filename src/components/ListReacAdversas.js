import React, { useState } from "react";
import { useForm } from "react-hook-form";

export default function ListReacAdversas(props) {
    const { register, handleSubmit, errors } = useForm();
    const [isEditing, setEditing] = useState(false);
 
    const onSubmit = data => {
        props.editReacAdversa(props.id, data)
        props.setEditando(false);
        setEditing(false);
    };

    const editingTemplate = (
    <form className="stack-small" onSubmit={handleSubmit(onSubmit)}>
        <br/>
        <div className="card">
        <h5 className="card-header">Reaccion Adversa - {props.descripcion.slice(0, 15)}...</h5>
            <div className="card-body">
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="descripcion_edit">Descripción de la reacción adversa</label>
                        <textarea 
                        class="form-control" 
                        id="descripcion_edit" 
                        name="descripcion_edit" 
                        rows="2"
                        ref={register({
                        required: {
                            value: true, 
                            message: 'Vacunas y productos biológicos son requeridos'
                            }
                        })}
                        defaultValue={props.descripcion}
                        >
                        </textarea>
                        <span className="text-danger">
                            {errors?.descripcion_edit?.message}
                        </span>
                    </div>
                    <div className="form-group col-md-3">
                        <label htmlFor="fec_inicio_edit">Fecha de Inicio</label>
                        <input 
                            type="date" 
                            className="form-control" 
                            id="fec_inicio_edit" 
                            name="fec_inicio_edit" 
                            placeholder="Fecha de Inicio"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Fecha de Inicio es requerida'
                                }
                            })}
                            defaultValue={props.fec_inicio}
                        />
                        <span className="text-danger">
                            {errors?.fec_inicio_edit?.message}
                        </span>
                    </div>

                    <div className="form-group col-md-3">
                        <label htmlFor="fec_fin_edit">Fecha de Fin</label>
                        <input 
                            type="date" 
                            className="form-control" 
                            id="fec_fin_edit" 
                            name="fec_fin_edit" 
                            placeholder="Fecha de Fin"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Fecha de Fin es requerida'
                                }
                            })}
                            defaultValue={props.fec_fin}
                        />
                        <span className="text-danger">
                            {errors?.fec_fin_edit?.message}
                        </span>
                    </div>
                </div>

                <div className="form-row">
                    <div className="form-gruop">
                        <div className="col-md-3">
                            <button type="submit" className="btn btn-success">
                                Guardar
                            </button>
                        </div>
                    </div>
                    <div className="form-gruop">
                        <div className="col-md-4">
                            <button
                            type="button"
                            className="btn btn-success"
                            onClick={() => {
                                setEditing(false);
                                props.setEditando(false);
                            }}
                            >Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
);

const viewTemplate = (
    <div>
        <br/>
        <div className="card table-responsive">
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Fecha de Inicio</th>
                        <th scope="col">Fecha de Fin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{props.descripcion}</td>
                        <td>{props.fec_inicio}</td>
                        <td>{props.fec_fin}</td>
                        <td>
                            <button
                            type="button"
                            className="btn btn-info"
                            onClick={() => {
                                if(props.Editando === false){
                                    setEditing(true);
                                    props.setEditando(true);
                                }
                            }}
                            >
                                <span>Editar</span>
                            </button>
                        </td>
                        <td>
                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick={() => props.deleteReacAdversa(props.id)}
                        >
                            <span>Eliminar</span>
                        </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
);

    return <div>{isEditing ? editingTemplate : viewTemplate}</div>;
}