import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { nanoid } from "nanoid";
import ListMedConcomitantes from "./ListMedConcomitantes";
import Alert from "./Alert";

const MedConcomitantes = props => {
    const { register, handleSubmit, errors } = useForm();
    const [Editando, setEditando] = useState(false);
    const [Alerta, setAlerta] = useState(false);
    
    function deleteMedConcomitante(id) {
        const remainingMedConcomitantes = props.Concomitantes.filter(medoncomitante => id !== medoncomitante.id);
        props.setConcomitantes(remainingMedConcomitantes);
    }

    function editMedConcomitante(id, data) {
        const editedMedConcomitante = props.Concomitantes.map(medoncomitante => {
          if (id === medoncomitante.id) {
            return {...medoncomitante, nombre: data.nombre_edit, 
                                      dosis: data.dosis_edit,
                                      frecuencia: data.frecuencia_edit,
                                      via: data.via_edit,
                                      lote: data.lote_edit,
                                      motivo: data.motivo_edit,
                                      fec_inicio: data.fec_inicio_edit,
                                      fec_fin: data.fec_fin_edit
                   }
          }
          return medoncomitante;
        });
        props.setConcomitantes(editedMedConcomitante);
    }

    const ConcomitantesReverso = [];
    for(let i = props.Concomitantes.length - 1; i >= 0; i--) {
        ConcomitantesReverso.push(props.Concomitantes[i]);
    }

    const medconcomitantesList = ConcomitantesReverso
    .map(medoncomitante => (
      <ListMedConcomitantes
        id={medoncomitante.id}
        nombre={medoncomitante.nombre}
        dosis={medoncomitante.dosis}
        frecuencia={medoncomitante.frecuencia}
        via={medoncomitante.via}
        lote={medoncomitante.lote}
        motivo={medoncomitante.motivo}
        fec_inicio={medoncomitante.fec_inicio}
        fec_fin={medoncomitante.fec_fin}
        key={medoncomitante.id}
        deleteMedConcomitante={deleteMedConcomitante}
        editMedConcomitante={editMedConcomitante}
        Editando={Editando}
        setEditando={setEditando}
      />
    ));

    const onSubmit = data => {
        const medicamento = {
            id: "medC-" + nanoid(),
            nombre: data.med_concomitante_nombre,
            dosis: data.med_concomitante_dosis,
            frecuencia: data.med_concomitante_frecuencia,
            via: data.med_concomitante_via,
            lote: data.med_concomitante_lote,
            motivo: data.med_concomitante_motivo,
            fec_inicio: data.med_concomitante_inicio,
            fec_fin: data.med_concomitante_fin
        }
        props.setConcomitantes([...props.Concomitantes, medicamento]);
    };

    return (
        <div>
            <Alert Mensaje = {'un medicamento concomitente.'} Alerta = {Alerta}  setAlerta = {setAlerta}/>
            <form onSubmit={handleSubmit(onSubmit)}>
            <br/>
                <div className="card">
                    <h5 className="card-header">Medicamentos concomitantes</h5>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group col-md-5">
                                <label htmlFor="med_concomitante_nombre">Nombre comercial o genérico</label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    id="med_concomitante_nombre" 
                                    name="med_concomitante_nombre" 
                                    placeholder="Nombre comercial o genérico"
                                    autoComplete="off"
                                    ref={register({
                                    required: {
                                        value: true, 
                                        message: 'Nombre comercial o genérico es requerido'
                                        }
                                    })}
                                />
                                <span className="text-danger">
                                    {errors?.med_concomitante_nombre?.message}
                                </span>
                            </div>

                            <div className="form-group col-md-2">
                            <label htmlFor="med_concomitante_dosis">Dosis</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_concomitante_dosis" 
                                name="med_concomitante_dosis" 
                                placeholder="Dosis"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Dosis es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_dosis?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-2">
                            <label htmlFor="med_concomitante_frecuencia">Frecuencia</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_concomitante_frecuencia" 
                                name="med_concomitante_frecuencia" 
                                placeholder="Frecuencia"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Frecuencia es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_frecuencia?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-3">
                            <label htmlFor="med_concomitante_via">Vía de Administración</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_concomitante_via" 
                                name="med_concomitante_via" 
                                placeholder="Vía de Administración"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Vía de Administración es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_via?.message}
                            </span>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-1">
                            <label htmlFor="med_concomitante_lote">Lote</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_concomitante_lote" 
                                name="med_concomitante_lote" 
                                placeholder="Lote"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Lote es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_lote?.message}
                            </span>
                            </div>
                            
                            <div className="form-group col-md-5">
                            <label htmlFor="med_concomitante_motivo">Motivo de prescripción</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_concomitante_motivo" 
                                name="med_concomitante_motivo" 
                                placeholder="Motivo de prescripción"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Motivo de prescripción es requerido'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_motivo?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-3">
                            <label htmlFor="med_concomitante_inicio">Fecha de Inicio</label>
                            <input 
                                type="date" 
                                className="form-control" 
                                id="med_concomitante_inicio" 
                                name="med_concomitante_inicio" 
                                placeholder="Fecha de Inicio"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Fecha de Inicio es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_inicio?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-3">
                            <label htmlFor="med_concomitante_fin">Fecha de Fin</label>
                            <input 
                                type="date" 
                                className="form-control" 
                                id="med_concomitante_fin" 
                                name="med_concomitante_fin" 
                                placeholder="Fecha de Fin"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Fecha de Fin es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_concomitante_fin?.message}
                            </span>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="submit" className="btn btn-success">
                                        Agregar medicamento concomitante
                                    </button>
                                </div>
                            </div>
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="button" onClick={()=>props.setPaso(2)} className="btn btn-success">
                                        Anterior
                                    </button>
                                </div>
                            </div>
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="button" onClick={()=>props.Concomitantes.length >= 1? props.setPaso(4) : setAlerta(true)} className="btn btn-success">
                                        Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {medconcomitantesList}
        </div>
    );
};
    
export default MedConcomitantes;
