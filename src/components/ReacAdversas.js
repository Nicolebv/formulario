import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { nanoid } from "nanoid";
import ListReacAdversa from "./ListReacAdversas";
import Alert from "./Alert";

const ReacAdversas = props => {
    const { register, handleSubmit, errors } = useForm();
    const [Editando, setEditando] = useState(false);
    const [Alerta, setAlerta] = useState(false);
    
    function deleteReacAdversa(id) {
        const remainingReacAdversa = props.Reacciones.filter(medoncomitante => id !== medoncomitante.id);
        props.setReacciones(remainingReacAdversa);
    }

    function editReacAdversa(id, data) {
        const editedReacAdversa = props.Reacciones.map(reaccion => {
          if (id === reaccion.id) {
            return {...reaccion, descripcion: data.descripcion_edit, 
                                      fec_inicio: data.fec_inicio_edit,
                                      fec_fin: data.fec_fin_edit
                   }
          }
          return reaccion;
        });
        props.setReacciones(editedReacAdversa);
    }

    const ReaccionesReverso = [];
    for(let i = props.Reacciones.length - 1; i >= 0; i--) {
        ReaccionesReverso.push(props.Reacciones[i]);
    }

    const medReaccionesList = ReaccionesReverso
    .map(reaccion => (
      <ListReacAdversa
        id={reaccion.id}
        descripcion={reaccion.descripcion}
        fec_inicio={reaccion.fec_inicio}
        fec_fin={reaccion.fec_fin}
        key={reaccion.id}
        deleteReacAdversa={deleteReacAdversa}
        editReacAdversa={editReacAdversa}
        Editando={Editando}
        setEditando={setEditando}
      />
    ));

    const onSubmit = data => {
        const reaccion = {
            id: "reac-" + nanoid(),
            descripcion: data.reaccion_descrip,
            fec_inicio: data.reaccion_inicio,
            fec_fin: data.reaccion_fin
        }
        props.setReacciones([...props.Reacciones, reaccion]);
    };

    return (
        <div>
            <Alert Mensaje = {'una descripción.'} Alerta = {Alerta}  setAlerta = {setAlerta}/>
            <form onSubmit={handleSubmit(onSubmit)}>
            <br/>
                <div className="card">
                    <h5 className="card-header">Descripción de la reacción adversa</h5>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label htmlFor="reaccion_descrip">Descripción</label>
                                <textarea 
                                className="form-control" 
                                id="reaccion_descrip" 
                                name="reaccion_descrip" 
                                rows="2"
                                ref={register({
                                    required: {
                                        value: true, 
                                        message: 'Descripción es requerida'
                                        }
                                    })}
                                >
                                </textarea>
                                <span className="text-danger">
                                    {errors?.reaccion_descrip?.message}
                                </span>
                            </div>
                            <div className="form-group col-md-3">
                                <label htmlFor="reaccion_inicio">Fecha de Inicio</label>
                                <input 
                                    type="date" 
                                    className="form-control" 
                                    id="reaccion_inicio" 
                                    name="reaccion_inicio" 
                                    placeholder="Fecha de Inicio"
                                    autoComplete="off"
                                    ref={register({
                                    required: {
                                        value: true, 
                                        message: 'Fecha de Inicio es requerida'
                                        }
                                    })}
                                />
                                <span className="text-danger">
                                    {errors?.reaccion_inicio?.message}
                                </span>
                            </div>

                            <div className="form-group col-md-3">
                                <label htmlFor="reaccion_fin">Fecha de Fin</label>
                                <input 
                                    type="date" 
                                    className="form-control" 
                                    id="reaccion_fin" 
                                    name="reaccion_fin" 
                                    placeholder="Fecha de Fin"
                                    autoComplete="off"
                                    ref={register({
                                    required: {
                                        value: true, 
                                        message: 'Fecha de Fin es requerida'
                                        }
                                    })}
                                />
                                <span className="text-danger">
                                    {errors?.reaccion_fin?.message}
                                </span>
                            </div>
                        </div>
                        
                        <div className="form-row">
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="submit" className="btn btn-success">
                                        Agregar reacción adversa
                                    </button>
                                </div>
                            </div>
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="button" onClick={()=>props.setPaso(3)} className="btn btn-success">
                                        Anterior
                                    </button>
                                </div>
                            </div>
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="button" onClick={()=>props.Reacciones.length >= 1? props.setPaso(5) : setAlerta(true)} className="btn btn-success">
                                        Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {medReaccionesList}
        </div>
    );
};
    
export default ReacAdversas;