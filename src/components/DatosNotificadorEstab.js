import React from "react";
import { useForm } from "react-hook-form";

function DatosNotificadorEstab(props) {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = data => {
    props.setNotificador({  nombre_ape: data.notificador_nombre_ape,
                            profesion: data.notificador_profesion,
                            direccion: data.notificador_direccion,
                            telefono: data.notificador_telefono,
                            correo: data.notificador_correo });
    props.setEstablecimiento({ nombre: data.estb_nombre,
                               localidad: data.estb_localidad});
    props.setReporteInicial(data.reporte_inicial);
    props.setPaso(7);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <br/>
      <div className="card">
        <h5 className="card-header">Datos del notificador</h5>
        <div className="card-body">
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="notificador_nombre_ape">Nombres y Apeliidos</label>
              <input type="text" 
                    className="form-control" 
                    id="notificador_nombre_ape" 
                    name="notificador_nombre_ape" 
                    placeholder="Nombres y Apellidos"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Nombres y Apellidos es requerido'
                          }
                      })}
                    defaultValue={props.Notificador.nombre_ape}
              />
              <span className="text-danger">
                  {errors?.notificador_nombre_ape?.message}
              </span>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="notificador_profesion">Profesión</label>
              <input type="text" 
                    className="form-control" 
                    id="notificador_profesion" 
                    name="notificador_profesion" 
                    placeholder="Profesión"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Profesión es requerida'
                          }
                      })}
                    defaultValue={props.Notificador.profesion}
              />
              <span className="text-danger">
                  {errors?.notificador_profesion?.message}
              </span>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="notificador_direccion">Dirección</label>
              <input type="text" 
                    className="form-control" 
                    id="notificador_direccion" 
                    name="notificador_direccion" 
                    placeholder="Dirección"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Dirección es requerida'
                          }
                      })}
                    defaultValue={props.Notificador.direccion}
              />
              <span className="text-danger">
                  {errors?.notificador_direccion?.message}
              </span>
            </div>
            <div className="form-group col-md-2">
              <label htmlFor="notificador_telefono">Teléfono</label>
              <input type="text" 
                      className="form-control" 
                      id="notificador_telefono" 
                      name="notificador_telefono" 
                      placeholder="Teléfono"
                      ref={register({
                        required: {
                            value: true, 
                            message: 'Teléfono es requerido'
                            }
                        })}
                      defaultValue={props.Notificador.telefono}
              />
              <span className="text-danger">
                  {errors?.notificador_telefono?.message}
              </span>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="notificador_correo">Correo electrónico</label>
              <input type="email" 
                      className="form-control" 
                      id="notificador_correo" 
                      name="notificador_correo" 
                      placeholder="Correo electrónico"
                      ref={register({
                        required: {
                            value: true, 
                            message: 'Correo electrónico es requerido'
                            }
                        })}
                      defaultValue={props.Notificador.correo}
              />
              <span className="text-danger">
                  {errors?.notificador_correo?.message}
              </span>
            </div>
          </div>
        </div>
      </div>

      <br/>
      <div className="card">
        <h5 className="card-header">Datos del establecimiento de salud</h5>
        <div className="card-body">
          <div className="form-row">
            <div className="form-group col-md-12">
              <label htmlFor="estb_nombre">Nombre del establecimiento de salud</label>
              <input type="text" 
                    className="form-control" 
                    id="estb_nombre" 
                    name="estb_nombre" 
                    placeholder="Nombre del establecimiento de salud"
                    ref={register({
                      required: {
                          value: true, 
                          message: 'Nombre del establecimiento de salud es requerido'
                          }
                      })}
                    defaultValue={props.Establecimiento.nombre}
              />
              <span className="text-danger">
                  {errors?.estb_nombre?.message}
              </span>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-12">
              <label htmlFor="estb_localidad">Localidad</label>
              <textarea
                className="form-control" 
                id="estb_localidad" 
                name="estb_localidad"
                rows="2"
                ref={register({
                  required: {
                      value: true, 
                      message: 'Localidad es requerida'
                      }
                  })}
                defaultValue={props.Establecimiento.localidad}>
              </textarea>
              <span className="text-danger">
                  {errors?.estb_localidad?.message}
              </span>
            </div>
          </div>
          
         </div>
      </div>

      <br/>
      <div className="card">
        <h5 className="card-header">Datos del reporte</h5>
        <div className="card-body">
            <div className="form-row">
                <fieldset className="form-group col-md-12">
                    <div className="row">
                        <legend className="col-form-label col-md-6">¿Reporte inicial?</legend>
                        <div className="col-sm-10">
                            <div className="form-check">
                                <input className="form-check-input" 
                                    type="radio" 
                                    name="reporte_inicial" 
                                    id="reporte_inicial_si" 
                                    value="Si"
                                    ref={register({
                                        required: {
                                            value: true, 
                                            message: 'Campo es requerido'
                                            }
                                        })}
                                    defaultChecked={props.ReporteInicial === 'Si'} 
                                />
                                <label className="form-check-label" htmlFor="reporte_inicial_si">
                                    Si es un Reporte Inicial
                                </label>
                            </div>
                            <div className="form-check">
                                <input className="form-check-input" 
                                    type="radio" 
                                    name="reporte_inicial" 
                                    id="reporte_inicial_no" 
                                    value="No"
                                    ref={register({
                                        required: {
                                            value: true, 
                                            message: 'Campo es requerido'
                                            }
                                        })}
                                    defaultChecked={props.ReporteInicial === 'No'} 
                                />
                                <label className="form-check-label" htmlFor="reporte_inicial_no">
                                No, este es un Reporte de Seguieminto
                                </label>
                            </div>
                            <span className="text-danger">
                                {errors?.reporte_inicial?.message}
                            </span>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
      </div>


      <br/>
        <div className="form-row">
            <div className="form-gruop">
                <div className="col-md">
                    <button type="submit" className="btn btn-success">
                        Procesar Formulario
                    </button>
                </div>
            </div>
            <div className="form-gruop">
                <div className="col-md">
                    <button type="button" onClick={()=>props.setPaso(5)} className="btn btn-success">
                        Anterior
                    </button>
                </div>
            </div>
        </div>

    </form>
  );
}

export default DatosNotificadorEstab;