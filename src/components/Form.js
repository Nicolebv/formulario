import React, { useState } from "react";
import DatosPaciente from "./DatosPaciente";
import MedSospechosos from "./MedSospechosos";
import MedConcomitantes from "./MedConcomitantes";
import ReacAdversas from "./ReacAdversas";
import OtrosDatos from "./OtrosDatos";
import DatosNotificadorEstab from "./DatosNotificadorEstab";
import ProcesarFormulario from "./ProcesarFormulario";
import Alert from "./Alert";

function Form() {
    const [Paso, setPaso] = useState(1);
    const [Paciente, setPaciente] = useState([]);
    const [Sospechosos, setSospechosos] = useState([]);
    const [Concomitantes, setConcomitantes] = useState([]);
    const [Reacciones, setReacciones] = useState([]);
    const [Manejo, setManejo] = useState([]);
    const [Resultado, setResultado] = useState([]);
    const [Gravedad, setGravedad] = useState([]);
    const [Antecedentes, setAntecedentes] = useState();
    const [Notificador, setNotificador] = useState([]);
    const [Establecimiento, setEstablecimiento] = useState([]);
    const [ReporteInicial, setReporteInicial] = useState();

    switch (Paso) {
        case 1:
            return (
                <DatosPaciente
                setPaso={setPaso}
                Paciente={Paciente}
                setPaciente={setPaciente}
                />
            );
        case 2:
            return (
                <MedSospechosos
                setPaso={setPaso}
                Sospechosos={Sospechosos}
                setSospechosos={setSospechosos}
                />
            );
        case 3:
            return (
                <MedConcomitantes
                setPaso={setPaso}
                Concomitantes={Concomitantes}
                setConcomitantes={setConcomitantes}
                />
            );
        case 4:
            console.log(Reacciones);
            return (
                <ReacAdversas
                setPaso={setPaso}
                Reacciones={Reacciones}
                setReacciones={setReacciones}
                />
            );
        case 5:
            return (
                <OtrosDatos
                setPaso={setPaso}
                Manejo={Manejo}
                setManejo={setManejo}
                Resultado={Resultado}
                setResultado={setResultado}
                Gravedad={Gravedad}
                setGravedad={setGravedad}
                Antecedentes={Antecedentes}
                setAntecedentes={setAntecedentes}
                />
            );
        case 6:
            return (
                <DatosNotificadorEstab
                setPaso={setPaso}
                Notificador={Notificador}
                setNotificador={setNotificador}
                Establecimiento={Establecimiento}
                setEstablecimiento={setEstablecimiento}
                ReporteInicial={ReporteInicial}
                setReporteInicial={setReporteInicial}
                />
            );
        case 7:
            return (
                <ProcesarFormulario
                setPaso={setPaso}
                Paciente={Paciente}
                Sospechosos={Sospechosos}
                Concomitantes={Concomitantes}
                Reacciones={Reacciones}
                Manejo={Manejo}
                Resultado={Resultado}
                Gravedad={Gravedad}
                Antecedentes={Antecedentes}
                Notificador={Notificador}
                Establecimiento={Establecimiento}
                ReporteInicial={ReporteInicial}
                />
            );
        case 8:
            return (
                <Alert
                />
            );
        default:
            return <div>Pagina no encontrada</div>;
    }   
}

export default Form;