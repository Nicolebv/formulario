import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { nanoid } from "nanoid";
import ListMedSospechosos from "./ListMedSospechosos";
import Alert from "./Alert";

const MedSospechosos = props => {
    const { register, handleSubmit, errors } = useForm();
    const [Editando, setEditando] = useState(false);
    const [Alerta, setAlerta] = useState(false);


    function deleteMedSospechoso(id) {
        const remainingMedSospechosos = props.Sospechosos.filter(medsospechoso => id !== medsospechoso.id);
        props.setSospechosos(remainingMedSospechosos);
    }

    function editMedSospechoso(id, data) {
        const editedMedSospechoso = props.Sospechosos.map(medsospechoso => {
          if (id === medsospechoso.id) {
            return {...medsospechoso, nombre: data.nombre_edit, 
                                      dosis: data.dosis_edit,
                                      frecuencia: data.frecuencia_edit,
                                      via: data.via_edit,
                                      lote: data.lote_edit,
                                      motivo: data.motivo_edit,
                                      fec_inicio: data.fec_inicio_edit,
                                      fec_fin: data.fec_fin_edit,
                                      vacunas: data.vacunas_edit
                   }
          }
          return medsospechoso;
        });
        props.setSospechosos(editedMedSospechoso);
    }

    const SospechososReverso = [];
    for(let i = props.Sospechosos.length - 1; i >= 0; i--) {
        SospechososReverso.push(props.Sospechosos[i]);
    }

    const medsospechosoList = SospechososReverso
    .map(medsospechoso => (
      <ListMedSospechosos
        id={medsospechoso.id}
        nombre={medsospechoso.nombre}
        dosis={medsospechoso.dosis}
        frecuencia={medsospechoso.frecuencia}
        via={medsospechoso.via}
        lote={medsospechoso.lote}
        motivo={medsospechoso.motivo}
        fec_inicio={medsospechoso.fec_inicio}
        fec_fin={medsospechoso.fec_fin}
        vacunas={medsospechoso.vacunas}
        key={medsospechoso.id}
        deleteMedSospechoso={deleteMedSospechoso}
        editMedSospechoso={editMedSospechoso}
        Editando={Editando}
        setEditando={setEditando}
      />
    ));

    const onSubmit = data => {
        const medicamento = {
            id: "medS-" + nanoid(),
            nombre: data.med_sospechoso_nombre,
            dosis: data.med_sospechoso_dosis,
            frecuencia: data.med_sospechoso_frecuencia,
            via: data.med_sospechoso_via,
            lote: data.med_sospechoso_lote,
            motivo: data.med_sospechoso_motivo,
            fec_inicio: data.med_sospechoso_inicio,
            fec_fin: data.med_sospechoso_fin,
            vacunas: data.med_sospechoso_vacunas_probio
        }
        props.setSospechosos([...props.Sospechosos, medicamento]);
    };

    return (
        <div>
            <Alert Mensaje = {'un medicamento sospechoso.'} Alerta = {Alerta}  setAlerta = {setAlerta}/>
            <form onSubmit={handleSubmit(onSubmit)}>
            <br/>
                <div className="card">
                    <h5 className="card-header">Medicamentos sospechoso de la reacción adversa</h5>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group col-md-5">
                                <label htmlFor="med_sospechoso_nombre">Nombre comercial o genérico</label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    id="med_sospechoso_nombre" 
                                    name="med_sospechoso_nombre" 
                                    placeholder="Nombre comercial o genérico"
                                    autoComplete="off"
                                    ref={register({
                                    required: {
                                        value: true, 
                                        message: 'Nombre comercial o genérico es requerido'
                                        }
                                    })}
                                />
                                <span className="text-danger">
                                    {errors?.med_sospechoso_nombre?.message}
                                </span>
                            </div>

                            <div className="form-group col-md-2">
                            <label htmlFor="med_sospechoso_dosis">Dosis</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_sospechoso_dosis" 
                                name="med_sospechoso_dosis" 
                                placeholder="Dosis"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Dosis es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_dosis?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-2">
                            <label htmlFor="med_sospechoso_frecuencia">Frecuencia</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_sospechoso_frecuencia" 
                                name="med_sospechoso_frecuencia" 
                                placeholder="Frecuencia"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Frecuencia es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_frecuencia?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-3">
                            <label htmlFor="med_sospechoso_via">Vía de Administración</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_sospechoso_via" 
                                name="med_sospechoso_via" 
                                placeholder="Vía de Administración"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Vía de Administración es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_via?.message}
                            </span>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-1">
                            <label htmlFor="med_sospechoso_lote">Lote</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_sospechoso_lote" 
                                name="med_sospechoso_lote" 
                                placeholder="Lote"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Lote es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_lote?.message}
                            </span>
                            </div>
                            
                            <div className="form-group col-md-5">
                            <label htmlFor="med_sospechoso_motivo">Motivo de prescripción</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="med_sospechoso_motivo" 
                                name="med_sospechoso_motivo" 
                                placeholder="Motivo de prescripción"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Motivo de prescripción es requerido'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_motivo?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-3">
                            <label htmlFor="med_sospechoso_inicio">Fecha de Inicio</label>
                            <input 
                                type="date" 
                                className="form-control" 
                                id="med_sospechoso_inicio" 
                                name="med_sospechoso_inicio" 
                                placeholder="Fecha de Inicio"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Fecha de Inicio es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_inicio?.message}
                            </span>
                            </div>

                            <div className="form-group col-md-3">
                            <label htmlFor="med_sospechoso_fin">Fecha de Fin</label>
                            <input 
                                type="date" 
                                className="form-control" 
                                id="med_sospechoso_fin" 
                                name="med_sospechoso_fin" 
                                placeholder="Fecha de Fin"
                                autoComplete="off"
                                ref={register({
                                required: {
                                    value: true, 
                                    message: 'Fecha de Fin es requerida'
                                    }
                                })}
                            />
                            <span className="text-danger">
                                {errors?.med_sospechoso_fin?.message}
                            </span>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-md-12">
                                <label htmlFor="med_sospechoso_vacunas_probio">Vacunas y productos biológicos  indicar  lote y fecha de vencimiento</label>
                                <textarea 
                                className="form-control" 
                                id="med_sospechoso_vacunas_probio" 
                                name="med_sospechoso_vacunas_probio" 
                                rows="2"
                                ref={register({
                                    required: {
                                        value: true, 
                                        message: 'Vacunas y productos biológicos requeridos'
                                    }
                                })}
                                >
                                </textarea>
                                <span className="text-danger">
                                    {errors?.med_sospechoso_vacunas_probio?.message}
                                </span>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="submit" className="btn btn-success">
                                        Agregar medicamento sospechoso
                                    </button>
                                </div>
                            </div>
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="button" onClick={()=>props.setPaso(1)} className="btn btn-success">
                                        Anterior
                                    </button>
                                </div>
                            </div>
                            <div className="form-gruop">
                                <div className="col-md-12">
                                    <button type="button" onClick={()=>props.Sospechosos.length >= 1? props.setPaso(3) : setAlerta(true)} className="btn btn-success">
                                        Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {medsospechosoList}
        </div>
    );
};
    
export default MedSospechosos;