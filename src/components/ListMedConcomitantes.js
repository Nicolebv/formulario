import React, { useState } from "react";
import { useForm } from "react-hook-form";

export default function ListMedConcomitantes(props) {
    const { register, handleSubmit, errors } = useForm();
    const [isEditing, setEditing] = useState(false);
 
    const onSubmit = data => {
        props.editMedConcomitante(props.id, data)
        props.setEditando(false);
        setEditing(false);
    };

    const editingTemplate = (
    <form className="stack-small" onSubmit={handleSubmit(onSubmit)}>
        <br/>
        <div className="card">
        <h5 className="card-header">Medicamento Concomitante - {props.nombre.slice(0, 15)}...</h5>
            <div className="card-body">
                <div className="form-row">
                    <div className="form-group col-md-5">
                        <label htmlFor="nombre_edit">Nombre comercial o genérico</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="nombre_edit" 
                            name="nombre_edit" 
                            placeholder="Nombre comercial o genérico"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Nombre comercial o genérico es requerido'
                                }
                            })}
                            defaultValue={props.nombre}
                        />
                        <span className="text-danger">
                            {errors?.nombre_edit?.message}
                        </span>
                    </div>

                    <div className="form-group col-md-2">
                        <label htmlFor="dosis_edit">Dosis</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="dosis_edit" 
                            name="dosis_edit" 
                            placeholder="Dosis"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Dosis es requerida'
                                }
                            })}
                            defaultValue={props.dosis}
                        />
                        <span className="text-danger">
                            {errors?.dosis_edit?.message}
                        </span>
                    </div>

                    <div className="form-group col-md-2">
                        <label htmlFor="frecuencia_edit">Frecuencia</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="frecuencia_edit" 
                            name="frecuencia_edit" 
                            placeholder="Frecuencia"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Frecuencia es requerida'
                                }
                            })}
                            defaultValue={props.frecuencia}
                        />
                        <span className="text-danger">
                            {errors?.frecuencia_edit?.message}
                        </span>
                    </div>

                    <div className="form-group col-md-3">
                        <label htmlFor="via_edit">Vía de Administración</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="via_edit" 
                            name="via_edit" 
                            placeholder="Vía de Administración"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Vía de Administración es requerida'
                                }
                            })}
                            defaultValue={props.via}
                        />
                        <span className="text-danger">
                            {errors?.via_edit?.message}
                        </span>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-1">
                        <label htmlFor="lote_edit">Lote</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="lote_edit" 
                            name="lote_edit" 
                            placeholder="Lote"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Lote es requerida'
                                }
                            })}
                            defaultValue={props.lote}
                        />
                        <span className="text-danger">
                            {errors?.lote_edit?.message}
                        </span>
                    </div>
                    
                    <div className="form-group col-md-5">
                        <label htmlFor="motivo_edit">Motivo de prescripción</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="motivo_edit" 
                            name="motivo_edit" 
                            placeholder="Motivo de prescripción"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Motivo de prescripción es requerido'
                                }
                            })}
                            defaultValue={props.motivo}
                        />
                        <span className="text-danger">
                            {errors?.motivo_edit?.message}
                        </span>
                    </div>

                    <div className="form-group col-md-3">
                        <label htmlFor="fec_inicio_edit">Fecha de Inicio</label>
                        <input 
                            type="date" 
                            className="form-control" 
                            id="fec_inicio_edit" 
                            name="fec_inicio_edit" 
                            placeholder="Fecha de Inicio"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Fecha de Inicio es requerida'
                                }
                            })}
                            defaultValue={props.fec_inicio}
                        />
                        <span className="text-danger">
                            {errors?.fec_inicio_edit?.message}
                        </span>
                    </div>

                    <div className="form-group col-md-3">
                        <label htmlFor="fec_fin_edit">Fecha de Fin</label>
                        <input 
                            type="date" 
                            className="form-control" 
                            id="fec_fin_edit" 
                            name="fec_fin_edit" 
                            placeholder="Fecha de Fin"
                            autoComplete="off"
                            ref={register({
                            required: {
                                value: true, 
                                message: 'Fecha de Fin es requerida'
                                }
                            })}
                            defaultValue={props.fec_fin}
                        />
                        <span className="text-danger">
                            {errors?.fec_fin_edit?.message}
                        </span>
                    </div>
                </div>

                <div className="form-row">
                    <div className="form-gruop">
                        <div className="col-md-3">
                            <button type="submit" className="btn btn-success">
                                Guardar
                            </button>
                        </div>
                    </div>
                    <div className="form-gruop">
                        <div className="col-md-4">
                            <button
                            type="button"
                            className="btn btn-success"
                            onClick={() => {
                                setEditing(false);
                                props.setEditando(false);
                            }}
                            >Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
);

const viewTemplate = (
    <div>
        <br/>
        <div className="card table-responsive">
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Dosis</th>
                        <th scope="col">Frecuencia</th>
                        <th scope="col">Vía</th>
                        <th scope="col">Lote</th>
                        <th scope="col">Motivo de prescripción</th>
                        <th scope="col">Fecha de Inicio</th>
                        <th scope="col">Fecha de Fin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{props.nombre}</td>
                        <td>{props.dosis}</td>
                        <td>{props.frecuencia}</td>
                        <td>{props.via}</td>
                        <td>{props.lote}</td>
                        <td>{props.motivo}</td>
                        <td>{props.fec_inicio}</td>
                        <td>{props.fec_fin}</td>
                        <td>
                            <button
                            type="button"
                            className="btn btn-info"
                            onClick={() => {
                                if(props.Editando === false){
                                    setEditing(true);
                                    props.setEditando(true);
                                }
                            }}
                            >
                                <span>Editar</span>
                            </button>
                        </td>
                        <td>
                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick={() => props.deleteMedConcomitante(props.id)}
                        >
                            <span>Eliminar</span>
                        </button>
                        </td>
                    </tr>
                </tbody>
                
            </table>
        </div>
    </div>
);

    return <div>{isEditing ? editingTemplate : viewTemplate}</div>;
}