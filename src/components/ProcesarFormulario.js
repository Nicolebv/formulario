import React, { useState } from "react";
import AlertEnviado from "./AlertEnviado";

const ProcesarFormulario = props => {
    const [Alerta, setAlerta] = useState(false);
    const data = [{
        Paciente: props.Paciente,
        MedSospechosos: props.Sospechosos,
        MedConcomitantes: props.Concomitantes,
        ReaccionesAdversas: props.Reacciones,
        ManejoEvAdverso: props.Manejo,
        ResultadoReaccion: props.Resultado,
        GravedadRAM: props.Gravedad,
        Antecedentes: props.Antecedentes,
        Notificador: props.Notificador,
        Establecimiento: props.Establecimiento,
        ReporteInicial: props.ReporteInicial
    }]

    const medsospechosoList = props.Sospechosos
    .map(medsospechoso => (
        <tr>
            <td>{medsospechoso.nombre}</td>
            <td>{medsospechoso.dosis}</td>
            <td>{medsospechoso.frecuencia}</td>
            <td>{medsospechoso.via}</td>
            <td>{medsospechoso.lote}</td>
            <td>{medsospechoso.motivo}</td>
            <td>{medsospechoso.fec_inicio}</td>
            <td>{medsospechoso.fec_fin}</td>
            <td>{medsospechoso.vacunas}</td>
        </tr>
    ));

    const medconcomitantesList = props.Concomitantes
    .map(concomitante => (
        <tr>
            <td>{concomitante.nombre}</td>
            <td>{concomitante.dosis}</td>
            <td>{concomitante.frecuencia}</td>
            <td>{concomitante.via}</td>
            <td>{concomitante.lote}</td>
            <td>{concomitante.motivo}</td>
            <td>{concomitante.fec_inicio}</td>
            <td>{concomitante.fec_fin}</td>
        </tr>
    ));

    const medreaccionesList = props.Reacciones
    .map(reaccion => (
        <tr>
            <td>{reaccion.descripcion}</td>
            <td>{reaccion.fec_inicio}</td>
            <td>{reaccion.fec_fin}</td>
        </tr>
    ));



    return (
        <div>
            <AlertEnviado Alerta = {Alerta}  setAlerta = {setAlerta}/>
            <br/>
            <div className="card table-responsive">
                <div className="card-body">
                    <h5 className="card-title">Datos del paciente</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombres</th>
                                <th scope="col">Apellidos</th>
                                <th scope="col">Sexo</th>
                                <th scope="col">Edad</th>
                                <th scope="col">Peso</th>
                                <th scope="col">Historia clínica</th>
                                <th scope="col">
                                    <button
                                        type="button"
                                        className="btn btn-info"
                                        onClick={() => props.setPaso(1)}
                                        >
                                            <span>Editar</span>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{props.Paciente.paciente_nombre}</td>
                                <td>{props.Paciente.paciente_apellido}</td>
                                <td>{props.Paciente.sexo}</td>
                                <td>{props.Paciente.edad}</td>
                                <td>{props.Paciente.peso}</td>
                                <td>{props.Paciente.historiaClinica}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <br/>
            <div className="card table-responsive">
                <div className="card-body">
                    <h5 className="card-title">Medicamentos sospechoso de la reacción adversa</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Dosis</th>
                                <th scope="col">Frecuencia</th>
                                <th scope="col">Vía</th>
                                <th scope="col">Lote</th>
                                <th scope="col">Motivo de prescripción</th>
                                <th scope="col">Fecha de Inicio</th>
                                <th scope="col">Fecha de Fin</th>
                                <th scope="col">Vacunas y productos</th>
                                <th scope="col">
                                    <button
                                        type="button"
                                        className="btn btn-info"
                                        onClick={() => props.setPaso(2)}
                                        >
                                            <span>Editar</span>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {medsospechosoList}
                        </tbody>
                    </table>
                </div>
            </div>

            <br/>
            <div className="card table-responsive">
                <div className="card-body">
                    <h5 className="card-title">Medicamentos concomitantes</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Dosis</th>
                                <th scope="col">Frecuencia</th>
                                <th scope="col">Vía</th>
                                <th scope="col">Lote</th>
                                <th scope="col">Motivo de prescripción</th>
                                <th scope="col">Fecha de Inicio</th>
                                <th scope="col">Fecha de Fin</th>
                                <th scope="col">
                                    <button
                                        type="button"
                                        className="btn btn-info"
                                        onClick={() => props.setPaso(3)}
                                        >
                                            <span>Editar</span>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {medconcomitantesList}
                        </tbody>
                    </table>
                </div>
            </div>

            <br/>
            <div className="card table-responsive">
                <div className="card-body">
                    <h5 className="card-title">Descripción de la reacción adversa</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Fecha de Inicio</th>
                                <th scope="col">Fecha de Fin</th>
                                <th scope="col">
                                    <button
                                        type="button"
                                        className="btn btn-info"
                                        onClick={() => props.setPaso(4)}
                                        >
                                            <span>Editar</span>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {medreaccionesList}
                        </tbody>
                    </table>
                </div>
            </div>

            <br/>
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">Manejo del evento adverso</h5>
                    <ul className="list-group">
                        <li className="list-group-item">
                            {props.Manejo.post_suspension === 'Si'? 'La reacción desapareció luego de la suspensión del medicamento' : 'La reacción NO desapareció luego de la suspensión del medicamento'}
                        </li>
                        <li className="list-group-item">
                        {props.Manejo.post_suspension === 'Si'? 'La reacción apareció luego de la re-administración del medicamento' : 'La reacción NO apareció luego de la re-administración del medicamento'}
                        </li>
                    </ul>

                    <br/>
                    <h5 className="card-title">Resultado de la reacción adversa</h5>
                    <ul className="list-group">
                        <li className="list-group-item">
                            {props.Resultado.secuela === 'No recuperado'? 'El paciente no se recupero de la reaccion adversa' : props.Resultado.secuela}
                        </li>
                        <li className="list-group-item">
                        {props.Resultado.muerte === 'No sabe'? 'No sabe si la reaccion adversa ocasiono la muerte del paciente' : props.Resultado.muerte}
                        </li>
                    </ul>
                    
                    <br/>
                    <h5 className="card-title">Gravedad de la RAM</h5>
                    <ul className="list-group">
                        <li className="list-group-item">
                            {props.Gravedad.grav_hospitalizacion === 'Si'? 'Produjo o prolongo la hospitalización del paciente' : 'No produjo o prolongo la hospitalización del paciente'}
                        </li>
                        <li className="list-group-item">
                        {props.Gravedad.grav_riesgo === 'Si'? 'Pone en riesgo la vida del paciente' : 'No pone en riesgo la vida del paciente'}
                        </li>
                        <li className="list-group-item">
                        {props.Gravedad.grav_incapacidad === 'Si'? 'Incapacidad persistente o grave' : 'No produjo una incapacidad persistente o grave'}
                        </li>
                        <li className="list-group-item">
                        {props.Gravedad.grav_muerte === 'Si'? 'Muerte del paciente' : 'No produjo la muerte del paciente'}
                        </li>
                        <li className="list-group-item">
                        {props.Gravedad.grav_anomalia === 'Si'? 'Anomalía congénita' : 'No produjo una anomalía congénita'}
                        </li>
                        <li className="list-group-item">
                        {props.Gravedad.grav_otros === 'Si'? 'Otras' : 'No produjo otras gravedades'}
                        </li>
                    </ul>

                    <br/>
                    <h5 className="card-title">Antecedentes relevantes</h5>
                    <hr/>
                    <p>{props.Antecedentes}</p>

                    <button
                        type="button"
                        className="btn btn-info"
                        onClick={() => props.setPaso(5)}
                        >
                            <span>Editar estos campos</span>
                    </button>
                </div>
            </div>
            <br/>
            <div className="card table-responsive">
                <div className="card-body">
                    <h5 className="card-title">Datos del notificador</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombres y Apellidos</th>
                                <th scope="col">Profesión</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Correo electrónico</th>
                                <th scope="col">
                                    <button
                                        type="button"
                                        className="btn btn-info"
                                        onClick={() => props.setPaso(6)}
                                        >
                                            <span>Editar estos campos</span>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{props.Notificador.nombre_ape}</td>
                                <td>{props.Notificador.profesion}</td>
                                <td>{props.Notificador.direccion}</td>
                                <td>{props.Notificador.telefono}</td>
                                <td>{props.Notificador.correo}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br/>

                    <h5 className="card-title">Datos del paciente</h5>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombres</th>
                                <th scope="col">Apellidos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{props.Establecimiento.nombre}</td>
                                <td>{props.Establecimiento.localidad}</td>
                            </tr>
                        </tbody>
                    </table>

                    <br/>
                    <h5 className="card-title">¿Reporte Inicial? {props.ReporteInicial === 'Si'? 'Si, este es un Reporte Inicial' : 'No, Este es un Reporte de Seguimiento'}</h5>

                </div>
            </div>

            <br/>
            <button
                type="button"
                className="btn btn-success"
                onClick={() => setAlerta(true)}
            >
                <span>Enviar Formulario</span>
            </button>
        </div>
    );
};
export default ProcesarFormulario;