import React from "react";

function Alert(props) {
  return (
    <div className="alerta" style={{pointerEvents:props.Alerta? 'auto' : 'none', opacity:props.Alerta? '1' : '0'}}>
        <div className="alerta-contenido card border-success">
            <h2 className="card-title">Alerta!</h2>
            <p className="card-text">Debe agregar al menos {props.Mensaje}</p>
            <div className="card-footer bg-transparent border-success">
                <button type="button" onClick={()=>{
                    props.setAlerta(false);
                }} className="btn btn-success">
                    Cerrar
                </button>
            </div>
            
        </div>  
    </div>  
  );
}

export default Alert;