import React from "react";

function AlertEnviado(props) {
  return (
    <div className="alerta" style={{pointerEvents:props.Alerta? 'auto' : 'none', opacity:props.Alerta? '1' : '0'}}>
        <div className="alerta-contenido card border-success">
            <h2 className="card-title">Alerta!</h2>
            <p className="card-text">El formulario ha sido enviado exitosamente</p>
            <div className="card-footer bg-transparent border-success">
                <a href="/" className="btn btn-success">
                    Cerrar
                </a>
            </div>
            
        </div>  
    </div>  
  );
}

export default AlertEnviado;